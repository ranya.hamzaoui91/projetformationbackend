package com.example.demo.dao;

import com.example.demo.models.Client;
import com.example.demo.models.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface iClient extends JpaRepository<Client,Long> {

    @Query("select m from Client m where m.firstName = :firstname")
    Client findByClientname(@Param("firstname") String firstname);
}
