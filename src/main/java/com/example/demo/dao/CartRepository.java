package com.example.demo.dao;

import com.example.demo.models.Cart;
import com.example.demo.models.Client;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CartRepository extends JpaRepository<Cart, Long> {
	Cart findByClient(Client client);
}
