package com.example.demo.dao;

import com.example.demo.models.Provider;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface iProvider extends JpaRepository<Provider,Long> {
}
