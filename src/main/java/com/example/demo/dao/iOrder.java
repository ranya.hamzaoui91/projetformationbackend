package com.example.demo.dao;

import com.example.demo.models.Client;
import com.example.demo.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface iOrder extends JpaRepository<Order,Long> {

      @Query("select c from Order c where c.client.firstName = :client ")
      List<Order> findByClient(@Param("client") Client client);


}
