package com.example.demo.dao;

import com.example.demo.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface iCategory extends JpaRepository<Category,Long> {
}
