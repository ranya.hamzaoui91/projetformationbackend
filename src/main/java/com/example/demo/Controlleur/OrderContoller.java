package com.example.demo.Controlleur;

import com.example.demo.dao.iClient;
import com.example.demo.dao.iOrder;
import com.example.demo.models.Client;
import com.example.demo.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/users/orders")
public class OrderContoller {
    @Autowired
    private iOrder iorder;

    @Autowired
    private iClient userRepository ;

     @GetMapping("/all")
    public List<Order> listOrder() {
        return iorder.findAll();
    }

    @PostMapping("/save")
    public Order saveOrder(@RequestBody Order o) {
        return iorder.save(o);
    }

    @PutMapping("/update/{Id}")
    public Order update(@RequestBody Order order, @PathVariable Long Id) {
        order.setId(Id);
        return iorder.saveAndFlush(order);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteOrder(@PathVariable(value = "Id") Long Id) {

        HashMap message= new HashMap();
          try{
              iorder.delete(Id);
               message.put("etat","order deleted");
              return message;
          }

    catch (Exception e) {
        message.put("etat","order not deleted");
        return message;
    }
    }

    @PostMapping("/submit/{username}")
    public ResponseEntity<Order> submit(@PathVariable String username) {
        Client client = userRepository.findByClientname(username);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        Order order = Order.createFromCart(client.getCart());
        iorder.save(order);
        return ResponseEntity.ok(order);
    }

    @GetMapping("/history/{username}")
    public ResponseEntity<List<Order>> getOrdersForUser(@PathVariable String username) {
        Client client = userRepository.findByClientname(username);
        if(client == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok(iorder.findByClient(client));
    }

}
