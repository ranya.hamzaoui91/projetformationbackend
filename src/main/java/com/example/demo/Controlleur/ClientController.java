package com.example.demo.Controlleur;

import com.example.demo.dao.CartRepository;
import com.example.demo.dao.IAuthority;
import com.example.demo.dao.iClient;
import com.example.demo.models.*;
import com.example.demo.security.TokenHelper;
import com.example.demo.security.auth.JwtAuthenticationRequest;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/users/clients")
public class ClientController {
    @Autowired
    private iClient iclient;

    @Autowired
    private IAuthority  iAuthority;

    @Autowired
    private StorageService  storage;

    @Autowired
    TokenHelper tokenHelper;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private CartRepository cartRepository;

    @GetMapping("/all")
    public List<Client> listClient() {
        return iclient.findAll();
    }

    @PostMapping("/save/{idAuthority}")
    public ResponseEntity<?> saveProvider(Client client, @PathVariable(value = "idAuthority") Long idAuthority,@RequestParam("file") MultipartFile file) {
        Authority authority = iAuthority.findOne(idAuthority);
        authority.setId(idAuthority);

        client.setEnabled(true);
        client.setAuthorities(authority);
        client.setPassword(hash(client.getPassword()));
        int i = (int) new Date().getTime();
        System.out.println("Integer : " + i);
        storage.store(file,i+file.getOriginalFilename());
        client.setImage(i+file.getOriginalFilename());

        Cart cart = new Cart();
        cartRepository.save(cart);
        client.setCart(cart);


        iclient.save(client);

        return ResponseEntity.ok(new UserTokenState(null, 0, client));

    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
                                                       HttpServletResponse response, Device device
    ) throws AuthenticationException, IOException {

        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        // Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // token creation
        Provider user = (Provider) authentication.getPrincipal();

        String jws = tokenHelper.generateToken(user, device);
        int expiresIn = tokenHelper.getExpiredIn(device);
        // Add cookie to response
        response.addCookie(createAuthCookie(jws, expiresIn));
        // Return the token
        return ResponseEntity.ok(new UserTokenState(jws, expiresIn, user));
    }


    private Cookie createAuthCookie(String jwt, int expiresIn) {
        Cookie authCookie = new Cookie(tokenHelper.AUTH_COOKIE, (jwt));
        authCookie.setPath("/");
        authCookie.setHttpOnly(true);
        authCookie.setMaxAge(expiresIn);
        return authCookie;
    }

    @PutMapping("/update/{Id}")
    public Client update(@RequestBody Client client, @PathVariable Long Id) {
        client.setId(Id);
        return iclient.saveAndFlush(client);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteClient(@PathVariable(value = "Id") Long Id) {

        HashMap message= new HashMap();
          try{
              iclient.delete(Id);
               message.put("etat","client deleted");
              return message;
          }

    catch (Exception e) {
        message.put("etat","client not deleted");
        return message;
    }

    }

    String hash(String password) {


        String hashedPassword = null;
        int i = 0;
        while (i < 5) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }
        return hashedPassword;
    }

}
