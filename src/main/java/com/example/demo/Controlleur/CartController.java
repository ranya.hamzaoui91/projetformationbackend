package com.example.demo.Controlleur;

import com.example.demo.dao.CartRepository;
import com.example.demo.dao.iClient;
import com.example.demo.dao.iProduct;
import com.example.demo.models.Cart;
import com.example.demo.models.Client;
import com.example.demo.models.ModifyCartRequest;
import com.example.demo.models.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;
import java.util.function.LongFunction;
import java.util.stream.IntStream;

@RestController
@RequestMapping("/users/cart")
public class CartController {
	
	@Autowired
	private iClient userRepository;
	
	@Autowired
	private CartRepository cartRepository;
	
	@Autowired
	private iProduct itemRepository;

	@PostMapping("/removeFromCart")
	public ResponseEntity<Cart> removeFromcart(@RequestBody ModifyCartRequest request) {
		Client client = userRepository.findByClientname(request.getUsername());
		if(client == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Product item = itemRepository.findOne(request.getItemId());
		if(item==null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Cart cart = client.getCart();
		IntStream.range(0, request.getQuantity())
			.forEach(i -> cart.removeProduct(item));
		cartRepository.save(cart);
		return ResponseEntity.ok(cart);
	}

	@PostMapping("/addtocart")
	public ResponseEntity<?> addTocart(@RequestBody ModifyCartRequest request) {
		Client client = userRepository.findByClientname(request.getUsername());
		if(client == null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}
		Product item = itemRepository.findOne(request.getItemId());
		if(item ==null) {
			return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
		}

		Cart cart = client.getCart();
		cart.addProduct(item);
		IntStream.range(0, request.getQuantity()).forEach(i -> cart.addProduct(item));
		cartRepository.save(cart);
		return ResponseEntity.ok(cart);
	}

	@GetMapping("/all")
	public List<Cart> addTocart2() {
		return cartRepository.findAll();
	}
}
