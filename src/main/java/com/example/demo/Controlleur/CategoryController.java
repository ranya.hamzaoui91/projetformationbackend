package com.example.demo.Controlleur;

import com.example.demo.dao.iCategory;
import com.example.demo.models.Category;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("/users/category")
public class CategoryController {
    @Autowired
    private iCategory icategory;
    @GetMapping("/all")
    public List<Category> listCategory(){
        return icategory.findAll();
    }
    @PostMapping("/save")
    public Category saveCategory(@RequestBody Category c){
        return icategory.save(c);
    }
    @PutMapping("/update/{Id}")
    public Category update(@RequestBody Category category, @PathVariable Long Id){
        category.setId(Id);
        return icategory.saveAndFlush(category);
    }
    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> delete(@PathVariable Long Id){
        HashMap msg=new HashMap();
        try{
            icategory.delete(Id);
           msg.put("etat", "category deleted");
            return msg;
        }
        catch (Exception e){
            msg.put("etat", "category not deleted");
            return msg;
        }
    }



}