package com.example.demo.Controlleur;

import com.example.demo.dao.iSubCategory;
import com.example.demo.models.SubCategory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("users/subcategory")

public class SubCategoryController {
    @Autowired
    private iSubCategory isubcategory;


    @GetMapping("/all")
    public List<SubCategory> listPSubCategory() {
        return isubcategory.findAll();
    }

    @PostMapping("/save")
    public SubCategory saveSubCategory(@RequestBody SubCategory p) {
        return isubcategory.save(p);
    }

    @PutMapping("/update/{Id}")
    public SubCategory update(@RequestBody SubCategory subcategory, @PathVariable Long Id) {
        subcategory.setId(Id);
        return isubcategory.saveAndFlush(subcategory);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteSubCategory(@PathVariable(value = "Id") Long Id) {

        HashMap message= new HashMap();
          try{
              isubcategory.delete(Id);
               message.put("etat","subcategory deleted");
              return message;
          }

    catch (Exception e) {
        message.put("etat","subcategory not deleted");
        return message;
    }


    }

}
