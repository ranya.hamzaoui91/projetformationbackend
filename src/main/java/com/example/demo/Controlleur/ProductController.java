package com.example.demo.Controlleur;

import com.example.demo.dao.iProduct;
import com.example.demo.dao.iSubCategory;
import com.example.demo.models.Product;
import com.example.demo.models.SubCategory;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@CrossOrigin("*")
@RequestMapping("users/products")

public class ProductController {

    @Autowired
    private iProduct iproduct;

    @Autowired
    private iSubCategory isubcategory;

    @Autowired
    private StorageService storage;

    @GetMapping("/all")
    public List<Product> listProduct() {
        return iproduct.findAll();
    }

    @PostMapping("/save/{idSub}")
    public Product saveProduct(Product p,@PathVariable Long idSub,  @RequestParam("file") MultipartFile file) {
        int i = (int) new Date().getTime();
        System.out.println("Integer : " + i);
        storage.store(file,i+file.getOriginalFilename());
        SubCategory subcat= isubcategory.findOne(idSub);
        p.setSubCategory(subcat);
        p.setPicture(i+file.getOriginalFilename());
        return iproduct.save(p);
    }

    @PutMapping("/update/{Id}")
    public Product update(@RequestBody Product product, @PathVariable Long Id) {
        product.setId(Id);
        return iproduct.saveAndFlush(product);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String,String> deleteProduct(@PathVariable(value = "Id") Long Id) {

        HashMap message= new HashMap();
          try{
              iproduct.delete(Id);
               message.put("etat","product deleted");
              return message;
          }

    catch (Exception e) {
        message.put("etat","product not deleted");
        return message;
    }
    }

    @GetMapping("/chercher")
    public List<Product> findProductbyName(String name) {
        return iproduct.chercher("%"+name+"%");
    }

    @GetMapping("/getByPage")
    public Page<Product> getProductByPage(int page, int size) {
        return iproduct.findbypage(new PageRequest(page,size));
    }


}