package com.example.demo.Controlleur;

import com.example.demo.common.DeviceProvider;
import com.example.demo.dao.IAuthority;
import com.example.demo.dao.iProvider;
import com.example.demo.models.*;
import com.example.demo.security.TokenHelper;
import com.example.demo.security.auth.JwtAuthenticationRequest;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import sun.tracing.ProviderSkeleton;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping("/providers")
public class ProviderController {
    @Autowired
    private iProvider iprovider;

    @Autowired
    private IAuthority iAuthority;

    @Autowired
    private StorageService storage;

    @Autowired
    private TokenHelper tokenHelper;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private UserDetailsService userDetailsService;

    @Autowired
    private DeviceProvider deviceProvider;

    @GetMapping("/all")

    public List<Provider> listProvider() {
        return iprovider.findAll();
    }

    @PostMapping("/save/{idAuthority}")
    public ResponseEntity<?> saveProvider(Provider p, @PathVariable(value = "idAuthority") Long idAuthority,
                                          @RequestParam("file") MultipartFile file) {
        Authority authority = iAuthority.findOne(idAuthority);
        authority.setId(idAuthority);
        p.setEnabled(true);
        p.setAuthorities(authority);
        p.setPassword(hash(p.getPassword()));
        int i = (int) new Date().getTime();
        System.out.println("Integer : " + i);
        storage.store(file,i+file.getOriginalFilename());
        p.setImage(i+file.getOriginalFilename());

        iprovider.save(p);

        return ResponseEntity.ok(new UserTokenState(null, 0, p));
    }


    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody JwtAuthenticationRequest authenticationRequest,
                                                       HttpServletResponse response, Device device
    ) throws AuthenticationException, IOException {

        // Perform the security
        final Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(
                        authenticationRequest.getUsername(),
                        authenticationRequest.getPassword()
                )
        );

        // Inject into security context
        SecurityContextHolder.getContext().setAuthentication(authentication);

        // token creation
        User provider = (User) authentication.getPrincipal();

        String jws = tokenHelper.generateToken(provider, device);
        int expiresIn = tokenHelper.getExpiredIn(device);
        // Add cookie to response
        response.addCookie(createAuthCookie(jws, expiresIn));
        // Return the token
        return ResponseEntity.ok(new UserTokenState(jws, expiresIn, provider));
    }


    private Cookie createAuthCookie(String jwt, int expiresIn) {
        Cookie authCookie = new Cookie(tokenHelper.AUTH_COOKIE, (jwt));
        authCookie.setPath("/");
        authCookie.setHttpOnly(true);
        authCookie.setMaxAge(expiresIn);
        return authCookie;
    }

    @PutMapping("/update/{Id}")
    public Provider update(@RequestBody Provider provider, @PathVariable Long Id) {
        provider.setId(Id);
        return iprovider.saveAndFlush(provider);
    }

    @DeleteMapping("/delete/{Id}")
    public HashMap<String, String> deleteProvider(@PathVariable(value = "Id") Long Id) {

        HashMap message = new HashMap();
        try {
            iprovider.delete(Id);
            message.put("etat", "provider deleted");
            return message;
        } catch (Exception e) {
            message.put("etat", "provider not deleted");
            return message;
        }


    }

    String hash(String password) {


        String hashedPassword = null;
        int i = 0;
        while (i < 5) {
            BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
            hashedPassword = passwordEncoder.encode(password);
            i++;
        }
        return hashedPassword;
    }
}
