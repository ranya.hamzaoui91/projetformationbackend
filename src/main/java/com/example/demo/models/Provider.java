package com.example.demo.models;

import javax.persistence.*;

@Entity
@Table(name="providers")
public class Provider extends User{


    private String company;


    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }
}
