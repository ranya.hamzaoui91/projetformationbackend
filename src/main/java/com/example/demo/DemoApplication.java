package com.example.demo;

import com.example.demo.dao.IAuthority;
import com.example.demo.dao.IUser;
import com.example.demo.models.Authority;
import com.example.demo.models.User;
import com.example.demo.utils.StorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
@EnableJpaAuditing
public class DemoApplication implements CommandLineRunner {

	@Autowired
	private StorageService storage;

	@Autowired
	private IUser iUser;

	@Autowired
	private IAuthority iAuthority;

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}
	@Override
	public void run(String... args) throws Exception {

		 //storage.init();
/*
		Authority auth=new Authority();
		Long Id ;
		Id= Long.valueOf(3);
		auth.setId(Id);

		User user = new User();
		user.setFirstName("ahmed");
		user.setLastName("ahmmed");
		user.setUsername("ahmed");
		user.setPassword(hash("ahmed"));
		user.setEnabled(true);
		user.setAuthorities(auth);
		iUser.save(user);


*/
//        Authority authority=new Authority();
//        authority.setName("ADMIN");
//        iAuthority.save(authority);
//
//		Authority authorityuser=new Authority();
//		authorityuser.setName("USER");
//		iAuthority.save(authorityuser);
//
//		Authority authoritysuperadmin=new Authority();
//		authoritysuperadmin.setName("SUPER_ADMIN");
//		iAuthority.save(authoritysuperadmin);
//
//		Authority authorityproovider=new Authority();
//		authorityproovider.setName("PROVIDER");
//		iAuthority.save(authorityproovider);
//
//
//		User user = new User();
//        user.setFirstName("Houssem");
//        user.setLastName("Angoud");
//        user.setUsername("admin");
//        user.setPassword(hash("admin"));
//        user.setEnabled(true);
//        user.setAuthorities(authority);
//        iUser.save(user);

	}

	String hash(String password) {


		String hashedPassword = null;
		int i = 0;
		while (i < 5) {
			BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
			hashedPassword = passwordEncoder.encode(password);
			i++;
		}

		return hashedPassword;
	}
}
